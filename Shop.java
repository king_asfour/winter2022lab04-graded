import java.util.Scanner;

public class Shop{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		Scanner readerString = new Scanner(System.in);
		Manga[] userManga = new Manga[4];
		
			for(int i = 0; i < userManga.length;i++){
				
			System.out.println("Who is the author of this manga?");
				String newAuthor = readerString.nextLine();
			System.out.println(" ");
			
			System.out.println("What is the title of this book?");
				String newTitle = readerString.nextLine();
			System.out.println(" ");
			

			System.out.println("How many pages does it contain?");
				int newPages = reader.nextInt();
			System.out.println(" ");
			
			Manga myBook = new Manga(newAuthor,newTitle,newPages);
				userManga[i] = myBook;
				
				if(i < userManga.length - 1){
					System.out.println("-------------------- NEXT BOOK!!! --------------------");
			}
			System.out.println(" "); 
		}
		//print before updating 2 value of the book 
		System.out.println("The author of the last product is: " + userManga[3].getAuthor());
		System.out.println("The title of the last product is: " + userManga[3].getTitle());
		System.out.println("The last product contain " + userManga[3].getPages() + " pages");
		
		userManga[3].productMethod();
		
		System.out.println(" ");
		
		//print after updating 2 value of the book
		System.out.println("---------------UPDATE LAST BOOK!!!---------------");
		
		System.out.println("Who is the author of the last book");
		userManga[3].setAuthor(readerString.nextLine());
		System.out.println(" ");
		System.out.println("How many pages does the last book actually contain?");
		userManga[3].setPages(reader.nextInt());
		System.out.println(" ");
		
		
		System.out.println("The author of the last product is: " + userManga[3].getAuthor());
		System.out.println("The title of the last product is: " + userManga[3].getTitle());
		System.out.println("The last product contain " + userManga[3].getPages() + " pages");
		
		
		userManga[3].productMethod();
			
		}
}
/*
SOURCE:
Source: GeeksforGeeks
https://www.geeksforgeeks.org/why-is-scanner-skipping-nextline-after-use-of-other-next-functions/

how to use parseInt:
userManga[i].pages = Integer.parseInt(reader.nextInt());
*/