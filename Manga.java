public class Manga{
	
	private int pages;
	private String author;
	private String title;

	public Manga(String newAuthor, String newTitle, int newPages){
		this.pages = newPages;
		this.author = newAuthor;
		this.title = newTitle;
	}
	
	public int getPages(){
		return this.pages;
	}
	public String getAuthor(){
		return this.author;
	}
	public String getTitle(){
		return this.title;
	}
	public void setPages(int newValuePages){
		this.pages = newValuePages;
	}
	public void setAuthor(String newAuthor){
		this.author = newAuthor;
	}
	
	public void productMethod(){
		if(pages >= 400){
			System.out.println("This is a big manga! Larger than the average size!!");
		}else{
			System.out.println("that's an average sized manga!");
		}
	}
}